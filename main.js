import Vue from 'vue/dist/vue';

import TestComponent from './src/TestComponent.vue';
import ProviderComponent from './src/ProviderComponent.vue';
import ConsumerComponent from './src/ConsumerComponent.vue';
import ErrorBoundaryComponent from './src/ErrorBoundaryComponent.vue';

new Vue({
    components: {
        ConsumerComponent,
        ErrorBoundaryComponent,
        ProviderComponent,
        TestComponent
    },

    computed: {
        actions() {
            return {
                getUsers: this.getUsers
            };
        },
        state() {
            return {
                taggers: []
            };
        }
    },

    methods: {
        getUsers() {
            return fetch('https://reqres.in/api/users?page=2')
                .then(response => response.json())
                .then(json => Promise.resolve(json.data));
        }
    }
}).$mount('#app');